﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using BeverageApplication.Models;

using Microsoft.AspNetCore.Authorization;

namespace BeverageApplication.Controllers
{
    public class BeveragesController : Controller
    {
        private readonly BeverageApplicationContext _context;

        public BeveragesController(BeverageApplicationContext context)
        {
            _context = context;
        }

        // GET: Beverages
        [Authorize(Roles ="Admin")]
        public async Task<IActionResult> Index()
        {
            var BeverageApplicationContext = _context.Beverage.Include(f => f.BeverageTypeFkNavigation);
            return View(await BeverageApplicationContext.ToListAsync());
        }


        public async Task<IActionResult> IndexPublic(string name, int? beverageType, int? maxprice)
        {
            var beverages = from someBeverages in _context.Beverage
                        select someBeverages;

            if(!string.IsNullOrEmpty(name))
            {
                beverages = beverages.Where(f => f.Name.Contains(name));
            }

            if(beverageType.HasValue)
            {
                beverages = beverages.Where(f => f.BeverageTypeFk == beverageType);
            }

            if(maxprice.HasValue)
            {
                beverages = beverages.Where(f => f.Price <= maxprice);
            }
            ViewData["name"] = name;
            ViewData["BeverageTypeFk"] = new SelectList(_context.BeverageType, "BeverageTypePk", "Type", beverageType);
            ViewData["maxprice"] = maxprice;

            return View(await beverages.Include(f => f.BeverageTypeFkNavigation).OrderBy(f => f.Name).ToListAsync());
        }

        // GET: Beverages/Details/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var beverage = await _context.Beverage
                .Include(f => f.BeverageTypeFkNavigation)
                .FirstOrDefaultAsync(m => m.BeveragePk == id);
            if (beverage == null)
            {
                return NotFound();
            }

            return View(beverage);
        }

        // GET: Beverages/Create
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            ViewData["BeverageTypeFk"] = new SelectList(_context.BeverageType, "BeverageTypePk", "Type");
            return View();
        }

        // POST: Beverages/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create([Bind("Name,Category,Price,BeverageTypeFk,Summary,DateInTheaters")] Beverage beverage)
        {
            if (ModelState.IsValid)
            {
                _context.Add(beverage);
                await _context.SaveChangesAsync();
                TempData["message"] = $"{beverage.Name} added seccessfully";
                return RedirectToAction(nameof(Index));
            }
            ViewData["BeverageTypeFk"] = new SelectList(_context.BeverageType, "BeverageTypePk", "Type", beverage.BeverageTypeFk);
            return View(beverage);
        }

        // GET: Beverages/Edit/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var beverage = await _context.Beverage.FindAsync(id);
            if (beverage == null)
            {
                return NotFound();
            }
            ViewData["BeverageTypeFk"] = new SelectList(_context.BeverageType, "BeverageTypePk", "Type", beverage.BeverageTypeFk);
            return View(beverage);
        }

        // POST: Beverages/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("BeveragePk,Name,Category,Price,BeverageTypeFk,Summary,ImageName,DateInTheaters")] Beverage beverage)
        {
            if (id != beverage.BeveragePk)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(beverage);
                    TempData["message"] = $"{beverage.Name} updated seccessfully";
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BeverageExists(beverage.BeveragePk))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["BeverageTypeFk"] = new SelectList(_context.BeverageType, "BeverageTypePk", "Type", beverage.BeverageTypeFk);
            return View(beverage);
        }

        // GET: Beverages/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var beverage = await _context.Beverage
                .Include(f => f.BeverageTypeFkNavigation)
                .FirstOrDefaultAsync(m => m.BeveragePk == id);
            if (beverage == null)
            {
                return NotFound();
            }

            return View(beverage);
        }

        // POST: Beverages/Delete/5
        [Authorize(Roles ="Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var Beverage = await _context.Beverage.FindAsync(id);
            _context.Beverage.Remove(Beverage);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BeverageExists(int id)
        {
            return _context.Beverage.Any(e => e.BeveragePk == id);
        }
    }
}
