﻿using System;
using System.Collections.Generic;

namespace BeverageApplication.Models
{
    public partial class BeverageType
    {
        public BeverageType()
        {
            Beverage = new HashSet<Beverage>();
        }

        public int BeverageTypePk { get; set; }
        public string Type { get; set; }

        public virtual ICollection<Beverage> Beverage { get; set; }
    }
}
