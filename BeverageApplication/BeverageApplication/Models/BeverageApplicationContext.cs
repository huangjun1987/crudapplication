﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace BeverageApplication.Models
{
    public partial class BeverageApplicationContext : DbContext
    {
        public BeverageApplicationContext()
        {
        }

        public BeverageApplicationContext(DbContextOptions<BeverageApplicationContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Beverage> Beverage { get; set; }
        public virtual DbSet<BeverageType> BeverageType { get; set; }
        public virtual DbSet<Customer> Customer { get; set; }
        public virtual DbSet<Orders> Orders { get; set; }
        public virtual DbSet<Role> Role { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=SC-HUANG-PC0804;Database=BeverageApplication;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<Beverage>(entity =>
            {
                entity.HasKey(e => e.BeveragePk);

                entity.Property(e => e.BeveragePk).HasColumnName("BeveragePK");

                entity.Property(e => e.BeverageTypeFk).HasColumnName("BeverageTypeFK");

                entity.Property(e => e.Category)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Price).HasColumnType("decimal(18, 2)");

                entity.Property(e => e.Size)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.BeverageTypeFkNavigation)
                    .WithMany(p => p.Beverage)
                    .HasForeignKey(d => d.BeverageTypeFk)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Beverage_BeverageType");
            });

            modelBuilder.Entity<BeverageType>(entity =>
            {
                entity.HasKey(e => e.BeverageTypePk);

                entity.Property(e => e.BeverageTypePk).HasColumnName("BeverageTypePK");

                entity.Property(e => e.Type)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.HasKey(e => e.CustPk);

                entity.Property(e => e.CustPk).HasColumnName("CustPK");

                entity.Property(e => e.CustAddress).HasMaxLength(50);

                entity.Property(e => e.Email).HasMaxLength(50);

                entity.Property(e => e.FirstName).HasMaxLength(50);

                entity.Property(e => e.LastName).HasMaxLength(50);

                entity.Property(e => e.LoginName).HasMaxLength(50);

                entity.Property(e => e.Password).HasMaxLength(50);

                entity.Property(e => e.RoleFk).HasColumnName("RoleFK");

                entity.HasOne(d => d.RoleFkNavigation)
                    .WithMany(p => p.Customer)
                    .HasForeignKey(d => d.RoleFk)
                    .HasConstraintName("FK_Customer_Role");
            });

            modelBuilder.Entity<Orders>(entity =>
            {
                entity.HasKey(e => e.OrderPk)
                    .HasName("PK_Order");

                entity.Property(e => e.OrderPk).HasColumnName("OrderPK");

                entity.Property(e => e.BeverageFk).HasColumnName("BeverageFK");

                entity.Property(e => e.CustFk).HasColumnName("CustFK");

                entity.Property(e => e.OrderDateTime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("(getdate())");

                entity.Property(e => e.OrderStatus)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.BeverageFkNavigation)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.BeverageFk)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Order_Beverage");

                entity.HasOne(d => d.CustFkNavigation)
                    .WithMany(p => p.Orders)
                    .HasForeignKey(d => d.CustFk)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Order_Customer");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.HasKey(e => e.RolePk);

                entity.Property(e => e.RolePk).HasColumnName("RolePK");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);
            });
        }
    }
}
