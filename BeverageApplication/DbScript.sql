/****** Object:  Table [dbo].[Beverage]    Script Date: 12/11/2019 7:45:43 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Beverage](
	[BeveragePK] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Category] [nvarchar](50) NOT NULL,
	[Size] [nvarchar](50) NOT NULL,
	[BeverageTypeFK] [int] NOT NULL,
	[Price] [decimal](18, 2) NULL,
 CONSTRAINT [PK_Beverage] PRIMARY KEY CLUSTERED 
(
	[BeveragePK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Beverage]  WITH CHECK ADD  CONSTRAINT [FK_Beverage_BeverageType] FOREIGN KEY([BeverageTypeFK])
REFERENCES [dbo].[BeverageType] ([BeverageTypePK])
GO

ALTER TABLE [dbo].[Beverage] CHECK CONSTRAINT [FK_Beverage_BeverageType]
GO


GO

/****** Object:  Table [dbo].[BeverageType]    Script Date: 12/11/2019 7:46:25 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[BeverageType](
	[BeverageTypePK] [int] IDENTITY(1,1) NOT NULL,
	[Type] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_BeverageType] PRIMARY KEY CLUSTERED 
(
	[BeverageTypePK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO



/****** Object:  Table [dbo].[Role]    Script Date: 12/11/2019 7:47:46 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Role](
	[RolePK] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[RolePK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

GO

/****** Object:  Table [dbo].[Customer]    Script Date: 12/11/2019 7:46:50 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Customer](
	[CustPK] [int] IDENTITY(1,1) NOT NULL,
	[LastName] [nvarchar](50) NULL,
	[FirstName] [nvarchar](50) NULL,
	[Email] [nvarchar](50) NULL,
	[LoginName] [nvarchar](50) NULL,
	[Password] [nvarchar](50) NULL,
	[CustNumber] [bigint] NULL,
	[CustAddress] [nvarchar](50) NULL,
	[RoleFK] [int] NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[CustPK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Customer]  WITH CHECK ADD  CONSTRAINT [FK_Customer_Role] FOREIGN KEY([RoleFK])
REFERENCES [dbo].[Role] ([RolePK])
GO

ALTER TABLE [dbo].[Customer] CHECK CONSTRAINT [FK_Customer_Role]
GO



GO

/****** Object:  Table [dbo].[Orders]    Script Date: 12/11/2019 7:47:19 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Orders](
	[OrderPK] [int] IDENTITY(1,1) NOT NULL,
	[OrderDateTime] [datetime] NOT NULL,
	[Quantity] [int] NOT NULL,
	[OrderStatus] [nvarchar](50) NOT NULL,
	[CustFK] [int] NOT NULL,
	[BeverageFK] [int] NOT NULL,
 CONSTRAINT [PK_Order] PRIMARY KEY CLUSTERED 
(
	[OrderPK] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Orders] ADD  CONSTRAINT [DF_Order_OrderDateTime]  DEFAULT (getdate()) FOR [OrderDateTime]
GO

ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Order_Beverage] FOREIGN KEY([BeverageFK])
REFERENCES [dbo].[Beverage] ([BeveragePK])
GO

ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Order_Beverage]
GO

ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Order_Customer] FOREIGN KEY([CustFK])
REFERENCES [dbo].[Customer] ([CustPK])
GO

ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Order_Customer]
GO

INSERT INTO [dbo].[BeverageType]
           ([Type])
     VALUES
           ('Green Tea')
INSERT INTO [dbo].[BeverageType]
           ([Type])
     VALUES
           ('Black Tea')
INSERT INTO [dbo].[BeverageType]
           ([Type])
     VALUES
           ('Coffee')
INSERT INTO [dbo].[BeverageType]
           ([Type])
     VALUES
           ('Soda')
INSERT INTO [dbo].[BeverageType]
           ([Type])
     VALUES
           ('Water')
INSERT INTO [dbo].[Beverage]
           ([Name]
           ,[Category]
           ,[Size]
           ,[BeverageTypeFK]
           ,[Price])
     VALUES
           ('Gold Peak� Green Tea'
           ,'A1'
           ,'Small'
           ,1
           ,11)
INSERT INTO [dbo].[Beverage]
           ([Name]
           ,[Category]
           ,[Size]
           ,[BeverageTypeFK]
           ,[Price])
     VALUES
           ('Gold Peak� Black Tea'
           ,'A2'
           ,'Medium'
           ,2
           ,22)
INSERT INTO [dbo].[Beverage]
           ([Name]
           ,[Category]
           ,[Size]
           ,[BeverageTypeFK]
           ,[Price])
     VALUES
           ('Starbucks Coffee'
           ,'B1'
           ,'Large'
           ,3
           ,33)
INSERT INTO [dbo].[Beverage]
           ([Name]
           ,[Category]
           ,[Size]
           ,[BeverageTypeFK]
           ,[Price])
     VALUES
           ('Coke'
           ,'C1'
           ,'Extra Large'
           ,4
           ,44)