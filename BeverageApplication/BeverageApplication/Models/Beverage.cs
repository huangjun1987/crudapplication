﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BeverageApplication.Models
{
    public partial class Beverage
    {
        public Beverage()
        {
            Orders = new HashSet<Orders>();
        }

        public int BeveragePk { get; set; }
        [Required(ErrorMessage = "Input required")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Input required")]
        public string Category { get; set; }
        [Required(ErrorMessage = "Input required")]
        public string Size { get; set; }
        public int BeverageTypeFk { get; set; }
        [Required(ErrorMessage = "Input required")]
        [DisplayFormat(DataFormatString = "{0:c0}")]
        public decimal? Price { get; set; }

        public virtual BeverageType BeverageTypeFkNavigation { get; set; }
        public virtual ICollection<Orders> Orders { get; set; }
    }
}
