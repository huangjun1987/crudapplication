﻿using System.Collections.Generic;

namespace CrudApplication.Models
{
    public class AccountViewModel
    {
        public List<Order> Orders { get; set; }
    }
}