﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BeverageApplication.Models
{
    public partial class Orders
    {
        public int OrderPk { get; set; }
        [Display(Name = "Date")]
        [DataType(DataType.Date)]
        public DateTime OrderDateTime { get; set; }
        [Required(ErrorMessage = "Input required")]
        [Range(0, 100, ErrorMessage = "Enter a quantity between 0 and 100")]
        public int Quantity { get; set; }
        public string OrderStatus { get; set; }
        public int CustFk { get; set; }
        public int BeverageFk { get; set; }
        [Display(Name = "Beverage")]
        public virtual Beverage BeverageFkNavigation { get; set; }
        [Display(Name = "Customer")]
        public virtual Customer CustFkNavigation { get; set; }
    }
}
