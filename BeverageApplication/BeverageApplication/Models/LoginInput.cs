﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


using System.ComponentModel.DataAnnotations;

namespace BeverageApplication.Models
{
    public class LoginInput
    {

        [Required(ErrorMessage = "Input required")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Input required")]
        [UIHint("password")]
        public string Password { get; set; }

        public string ReturnURL { get; set; }
    }
}

