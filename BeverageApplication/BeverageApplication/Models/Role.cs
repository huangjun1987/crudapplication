﻿using System;
using System.Collections.Generic;

namespace BeverageApplication.Models
{
    public partial class Role
    {
        public Role()
        {
            Customer = new HashSet<Customer>();
        }

        public int RolePk { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Customer> Customer { get; set; }
    }
}
