﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BeverageApplication.Models
{
    public partial class Customer
    {
        public Customer()
        {
            Orders = new HashSet<Orders>();
        }

        public int CustPk { get; set; }
        [Required(ErrorMessage = "Input required")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Input required")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Input required")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required(ErrorMessage = "Input required")]
        [MaxLength(50)]
        [RegularExpression(@"^[a-zA-Z0-9]+$", ErrorMessage = "Alpha-numeric characters only.")]
        public string LoginName { get; set; }
        [Required(ErrorMessage = "Input required")]
        [UIHint("password")]
        public string Password { get; set; }
        public long? CustNumber { get; set; }
        public string CustAddress { get; set; }
        public int? RoleFk { get; set; }

        public virtual Role RoleFkNavigation { get; set; }
        public virtual ICollection<Orders> Orders { get; set; }
    }
}
