﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CrudApplication.Models
{
    public class HomeViewModel
    {
        public string BeverageName { get; set; }
        public string MinPrice { get; set; }
        public string MaxPrice { get; set; }
        public string BeverageType { get; set; }
        public Dictionary<string, string> AvailableBeverageTypes { get; set; }

        public List<Beverage> Beverages { get; set; }

        public HomeViewModel()
        {
            AvailableBeverageTypes = new Dictionary<string, string>();
            Beverages = new List<Beverage>();
        }
    }
}