﻿using CrudApplication.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Linq;
using System.Web.Mvc;

namespace CrudApplication.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var model = new HomeViewModel();
            SearchBeverages(ref model);
            return View(model);
        }

        private void SearchBeverages(ref HomeViewModel model)
        {
            using (var context = new CrudEntities())
            {
                if (model.AvailableBeverageTypes.Count()<1)
                {
                    model.AvailableBeverageTypes[""] = "--Select--";
                    foreach (var beverageType in context.BeverageTypes)
                    {
                        model.AvailableBeverageTypes[beverageType.BeverageTypePK.ToString()] = beverageType.Type;
                    }
                }

                var beveragesQuery = context.Beverages.Select(b=>b);

                if (!string.IsNullOrWhiteSpace(model.BeverageName))
                {
                    var name = model.BeverageName.Trim();
                    beveragesQuery = beveragesQuery.Where(b => b.Name.Contains(name));
                }
                if (!string.IsNullOrWhiteSpace(model.BeverageType))
                {
                    var beverageTypeInt = 0;
                    if (int.TryParse(model.BeverageType, out beverageTypeInt))
                    {                       
                        beveragesQuery = beveragesQuery.Where(b => b.BeverageTypeFK.Equals(beverageTypeInt));
                    }
                }
                decimal minPrice = 0M;
                if (decimal.TryParse(model.MinPrice, out minPrice))
                {
                    beveragesQuery = beveragesQuery.Where(b => b.Price>=minPrice);
                }
                decimal maxPrice = 0M;
                if (decimal.TryParse(model.MaxPrice, out maxPrice))
                {
                    beveragesQuery = beveragesQuery.Where(b => b.Price <= maxPrice);
                }
                model.Beverages = beveragesQuery.ToList();
            }
        }

        //
        // POST: /Home/Index
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Index(HomeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            SearchBeverages(ref model);
            return View(model);       
        }
        [Authorize]
        public ActionResult Order(int id)
        {
            using (var context = new CrudEntities())
            {
                if (!context.Beverages.Any(b=>b.BeveragePK.Equals(id)))
                {
                   return RedirectToAction("Index", "Home");
                }
                var order = new Order();
                var beverageName = context.Beverages.Where(b => b.BeveragePK.Equals(id)).Select(b => b.Name).FirstOrDefault();
                ViewBag.Message = $"Thank you for ordering {beverageName}";
                order.BeverageFK = id;
                return View(order);
            }
        }
        [Authorize]
        public ActionResult CancelOrder(int id)
        {
            using (var context = new CrudEntities())
            {
                var userName = User.Identity.GetUserName();
                var customerPK = context.Customers.Where(c => c.LoginName.Equals(userName, StringComparison.OrdinalIgnoreCase)).Select(c => c.CustPK).FirstOrDefault();
                var orderToDelete = context.Orders.FirstOrDefault(o => o.OrderPK.Equals(id) && o.CustFK.Equals(customerPK));
                if (orderToDelete == null)
                {
                    return RedirectToAction("Account", "Home");
                }
                context.Orders.Remove(orderToDelete);
                context.SaveChanges();
                return RedirectToAction("Account", "Home", new { notification = "Order was cancelled successfully!" });
            }
        }
        //
        // POST: /Home/Index
        [HttpPost]
        [Authorize]
        public ActionResult Order(Order model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var userName = User.Identity.GetUserName();
            using (var context = new CrudEntities())
            {
                var customer = context.Customers.Where(c => c.LoginName.Equals(userName)).FirstOrDefault();
                model.CustFK = customer.CustPK;
                model.OrderDateTime = DateTime.Now;
                model.OrderStatus = "Pending";
                context.Orders.Add(model);
                context.SaveChanges();
            }
            return RedirectToAction("Account", "Home", new { notification = "Order was created successfully!" });
        }

        [Authorize]
        public ActionResult Account(string notification)
        {
            var userName = User.Identity.GetUserName();
            ViewBag.Message = $"Hello {userName}";
            ViewBag.Notification = notification;

            var model = new AccountViewModel();
            using (var context = new CrudEntities())
            {
                model.Orders = context.Orders.Include("Customer").Include("Beverage").Where(o => o.Customer.LoginName.Equals(userName)).ToList();
            }
            
            return View(model);
        }

    }
}