﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CrudApplication;

namespace CrudApplication.Controllers
{
    [Authorize(Roles = "Admin")]
    public class BeverageTypesController : Controller
    {
        private CrudEntities db = new CrudEntities();

        // GET: BeverageTypes
        public ActionResult Index()
        {
            return View(db.BeverageTypes.ToList());
        }

        // GET: BeverageTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BeverageType beverageType = db.BeverageTypes.Find(id);
            if (beverageType == null)
            {
                return HttpNotFound();
            }
            return View(beverageType);
        }

        // GET: BeverageTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BeverageTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "BeverageTypePK,Type")] BeverageType beverageType)
        {
            if (ModelState.IsValid)
            {
                db.BeverageTypes.Add(beverageType);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(beverageType);
        }

        // GET: BeverageTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BeverageType beverageType = db.BeverageTypes.Find(id);
            if (beverageType == null)
            {
                return HttpNotFound();
            }
            return View(beverageType);
        }

        // POST: BeverageTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "BeverageTypePK,Type")] BeverageType beverageType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(beverageType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(beverageType);
        }

        // GET: BeverageTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BeverageType beverageType = db.BeverageTypes.Find(id);
            if (beverageType == null)
            {
                return HttpNotFound();
            }
            return View(beverageType);
        }

        // POST: BeverageTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BeverageType beverageType = db.BeverageTypes.Find(id);
            db.BeverageTypes.Remove(beverageType);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
