﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using Microsoft.EntityFrameworkCore;
using BeverageApplication.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Security.Claims;

//for authorization
using Microsoft.AspNetCore.Authorization;

namespace BeverageApplication.Controllers
{
    public class AccountController : Controller

    {
        private readonly BeverageApplicationContext _context;

        public AccountController(BeverageApplicationContext context)
        {
            _context = context;
        }

        public IActionResult Login(string returnURL)
        {
            returnURL = string.IsNullOrEmpty(returnURL) ? "~/Account" : returnURL;

            return View(new LoginInput { ReturnURL = returnURL });
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login([Bind("Username, Password, ReturnURL")]
        LoginInput loginInput)
        {
            if(ModelState.IsValid)
            {
                var aUser = await _context.Customer.Include(u=>
                u.RoleFkNavigation).FirstOrDefaultAsync(u=> u.LoginName ==
                    loginInput.Username && u.Password == loginInput.Password);

                if (aUser !=null)
                {
                    var claims = new List<Claim>();
                    claims.Add(new Claim(ClaimTypes.Name, aUser.FirstName));
                    claims.Add(new Claim(ClaimTypes.Sid, aUser.CustPk.ToString()));
                    claims.Add(new Claim(ClaimTypes.Role, aUser.RoleFkNavigation.Name));

                    var identity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);

                    var principal = new ClaimsPrincipal(identity);
                    await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);

                    return Redirect(loginInput.ReturnURL);
                }
                else
                {
                    ViewData["message"] = "Invalid credentials";
                }
            }
            return View(loginInput);
        }

        public async Task<RedirectToActionResult> Logout()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Index", "Home");
        }

        [Authorize]
        public async Task<IActionResult> Index()
        {
            int userPK = Int32.Parse(HttpContext.User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Sid).Value);
            var myOrders = _context.Orders.Include(r => r.BeverageFkNavigation).Where(r=>r.CustFk == userPK)
                .OrderBy(r=>r.BeverageFkNavigation.Name);

            var name = HttpContext.User.Claims.FirstOrDefault(u => u.Type == ClaimTypes.Name).Value;

            ViewData["namefirst"] = name;
            return View(await myOrders.ToListAsync());
            
        }

        public IActionResult SignUp()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> SignUp([Bind("LoginName, Password, FirstName, LastName,Email,CustNumber,CustAddress")] Customer aCustomer)
        {
            if (ModelState.IsValid){
                var aUser = await _context.Customer.FirstOrDefaultAsync(u => u.LoginName == aCustomer.LoginName);

                if(aUser is null)
                {
                    _context.Add(aCustomer);
                    await _context.SaveChangesAsync();

                    TempData["message"] = "Thanks for registering, please log in.";
                    return RedirectToAction("Login");

                }
                else
                {
                    ViewData["duplicatemessage"] = "Please choose a diferent username";
                }
            }

            return View(aCustomer);
        }

        [Authorize]
        public async Task<IActionResult> CreateOrder(int beveragepk)
        {
            var oneBeverage = await _context.Beverage.FirstOrDefaultAsync(f => f.BeveragePk == beveragepk);

            if(oneBeverage == null)
            {
                return RedirectToAction("IndexPublic", "Beverages");
            }

            ViewData["BeverageFk"] = beveragepk;
            ViewData["Name"] = oneBeverage.Name;
            return View();
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]

        public async Task<IActionResult> CreateOrder([Bind("Quantity, OrderRating, BeverageFk")] Orders order)
        {
            if(ModelState.IsValid)
            {
                //getting logged in user's ID attaching it to the Order
                int userPK = Int32.Parse(HttpContext.User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Sid).Value);
                order.CustFk = userPK;
                order.OrderStatus = "Pending";
                _context.Add(order);
                await _context.SaveChangesAsync();

                // get the Beverage that was just Ordered. e.g. aOrder.BeverageFk
                var orderedBeverage = await _context.Beverage.FirstOrDefaultAsync(f => f.BeveragePk == order.BeverageFk);
                TempData["message"] = $"Order of {orderedBeverage.Name} added successfully.";
                return RedirectToAction("Index", "Account");

            }
            else
            {
                return View();

            }
        }

        [Authorize]
        public async Task<IActionResult> DeleteOrder (int orderpk)
        {
            var order = await _context.Orders
                .Include(r => r.BeverageFkNavigation)
                .FirstOrDefaultAsync(r => r.OrderPk == orderpk);

            if (order == null)
            {
                return RedirectToAction("Index", "Account");
            }

            //prevent user from delete another user's Order
            int userPK = Int32.Parse(HttpContext.User.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Sid).Value);
            if (order.CustFk != userPK)
            {
                return RedirectToAction("Index", "Account");
            }

            _context.Remove(order);
            await _context.SaveChangesAsync();

            TempData["message"] = $"Order of {order.BeverageFkNavigation.Name} delete.";
            return RedirectToAction("Index", "Account");

        }

        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> OrdersAdmin()
        {
            var Orders = _context.Orders
                .Include(r => r.CustFkNavigation)
                .Include(r => r.BeverageFkNavigation)
                .OrderByDescending(r => r.OrderDateTime);

            return View(await Orders.ToListAsync());
        }

    }
}